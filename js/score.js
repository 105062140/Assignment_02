var scoreState ={
    preload: function() {
        game.load.image('background', 'asset/bg.png');
        game.load.audio('select', 'sound/select.mp3');
    },
    
    create: function() {
        this.background = game.add.sprite(0, 0, 'background');

        var leaderboard = game.add.text(232, game.height/2+30,'loading...' , { font: '25px Arial', fill: '#ffffff' });
        leaderboard.anchor.setTo(0.5, 0.5);

        var post = firebase.database().ref('list').orderByChild('score').limitToFirst(6);
        post.once('value', function(snapshot){
            var text1 = '';
            snapshot.forEach(function(childSnapshot){
                text1 += childSnapshot.val().name + '  ' +( -childSnapshot.val().score) + '\n';
            });
            leaderboard.text = text1;
        });

        var text2 = game.add.text(400, 350, 'Back', { font: '35px Arial', fill: '#fff' });
        text2.inputEnabled = true;
        text2.events.onInputUp.add(this.back);
    },
    
    back: function(target) {
        if(playSound){
            select.play();
        }
        game.state.start('menu');
    }
}
game.state.add('score', scoreState);