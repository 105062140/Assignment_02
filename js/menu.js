var select,powerup,powerdown,jump,
    playSound=true;
var menuState ={
    preload: function() {
        game.load.image('background', 'asset/bg.png');
        game.load.audio('select', 'sound/select.mp3');
    },
    
    create: function() {
        this.background = game.add.sprite(0, 0, 'background');
        select = game.add.audio('select');
        var style = {
            font: "48px Monospace",
            fill: "#00ff00",
            align: "center"
        };
        var text = game.add.text(game.width/2, game.height/2-100, '小朋友下樓梯', style);
        text.anchor.set(0.5);

        var text_start = game.add.text(120, 200, 'start', { font: '50px Arial', fill: '#fff' });
        text_start.inputEnabled = true;
        text_start.events.onInputUp.add(this.startGame);
        var text_board = game.add.text(120, 300, 'leaderboard', { font: '50px Arial', fill: '#fff' });
        text_board.inputEnabled = true;
        text_board.events.onInputUp.add(this.score);
    },
    
    startGame: function(target) {
        if(playSound){
            select.play();
        }
        game.state.start('main');
    },
    score: function(target) {
        if(playSound){
            select.play();
        }
        game.state.start('score');
    }
}

var game = new Phaser.Game(650, 450, Phaser.AUTO, 'canvas');
game.state.add('menu', menuState);
game.state.start('menu');