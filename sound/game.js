var select,powerup,powerdown,jump,
    playSound=true;
var titleScreen ={
    preload: function() {
        game.load.image('background', 'asset/bg.png');
        game.load.audio('select', 'sound/select.mp3');
    },
    
    create: function() {
        this.background = game.add.sprite(0, 0, 'background');
        select = game.add.audio('select');
        var style = {
            font: "48px Monospace",
            fill: "#00ff00",
            align: "center"
        };
        var text = game.add.text(game.width/2, game.height/2-100, '小朋友下樓梯', style);
        text.anchor.set(0.5);

        var text_start = game.add.text(120, 200, 'start', { font: '50px Arial', fill: '#fff' });
        text_start.inputEnabled = true;
        text_start.events.onInputUp.add(this.startGame);
        var text_board = game.add.text(120, 300, 'leaderboard', { font: '50px Arial', fill: '#fff' });
        text_board.inputEnabled = true;
        text_board.events.onInputUp.add(this.score);
    },
    
    startGame: function(target) {
        if(playSound){
            select.play();
        }
        game.state.start('main');
    },
    score: function(target) {
        if(playSound){
            select.play();
        }
        game.state.start('score');
    }
}
var scoreBoard ={
    preload: function() {
        game.load.image('background', 'asset/bg.png');
        game.load.audio('select', 'sound/select.mp3');
    },
    
    create: function() {
        this.background = game.add.sprite(0, 0, 'background');

        var leaderboard = game.add.text(232, game.height/2+30,'loading...' , { font: '25px Arial', fill: '#ffffff' });
        leaderboard.anchor.setTo(0.5, 0.5);

        var post = firebase.database().ref('list').orderByChild('score').limitToFirst(6);
        post.once('value', function(snapshot){
            var text1 = '';
            snapshot.forEach(function(childSnapshot){
                text1 += childSnapshot.val().name + '  ' +( -childSnapshot.val().score) + '\n';
            });
            leaderboard.text = text1;
        });

        var text2 = game.add.text(400, 350, 'Back', { font: '35px Arial', fill: '#fff' });
        text2.inputEnabled = true;
        text2.events.onInputUp.add(this.back);
    },
    
    back: function(target) {
        if(playSound){
            select.play();
        }
        game.state.start('title');
    }

}
var endState ={
    preload:function(){
        game.load.image('background', 'asset/bg2.jpg');
    },
    create: function() {
        var style = {
            font: "48px Monospace",
            fill: "#00ff00",
            align: "center"
        };
        var style2 = {
            font: "30px Monospace",
            fill: "#00ff00",
            align: "center"
        };
        this.background = game.add.sprite(0, 0, 'background');
        var player = prompt("Please enter your name", "");
        if(player!=null){
            var newpostref = firebase.database().ref('list').push();
                newpostref.set({
                    score: -mainState.score,
                    name: player
                });
        }

        var text1 = game.add.text(190, 100, 'Game Over', style);
        var text2 = game.add.text(170, 180, 'Your Score:'+mainState.score, style);

        var text_start = game.add.text(120, 300, 'again', { font: '50px Arial', fill: '#fff' });
        text_start.inputEnabled = true;
        text_start.events.onInputUp.add(this.play);
        var text_board = game.add.text(350, 300, 'menu', { font: '50px Arial', fill: '#fff' });
        text_board.inputEnabled = true;
        text_board.events.onInputUp.add(this.menu);
    },
    play: function () {
        if(playSound){
            select.play();
        }
        game.state.start('main');
        
    },
    menu: function () {
        if(playSound){
            select.play();
        }
        game.state.start('title');
    }

}
var mainState ={
    preload : function () {

        game.load.spritesheet('soundicons', 'asset/soundicons.png', 40, 40)
        game.load.spritesheet('player', 'asset/player.png', 32, 32);
        game.load.spritesheet('background', 'asset/bg.png', 400, 355);
        game.load.image('background2', 'asset/bg2.jpg');
        game.load.image('bar1', 'asset/bar1.png');
        game.load.image('bar2', 'asset/bar2.png');
        game.load.image('bar3', 'asset/bar3.png');
        game.load.image('wall', 'asset/wall.png');
        game.load.image('ceiling', 'asset/ceiling.png');
        game.load.image('normal', 'asset/normal.png');
        game.load.image('nails', 'asset/nails.png');
        game.load.spritesheet('conveyorRight', 'asset/conveyor_right.png', 96, 16);
        game.load.spritesheet('conveyorLeft', 'asset/conveyor_left.png', 96, 16);
        game.load.spritesheet('trampoline', 'asset/trampoline.png', 96, 22);
        game.load.spritesheet('fake', 'asset/fake.png', 96, 36);
        game.load.image('pixel', 'asset/red.jpg');
        game.load.image('undo', 'asset/undo.png');
        game.load.image('pause', 'asset/pause.png');
        game.load.image('life', 'asset/life.png');
        game.load.spritesheet('yellow', 'asset/yellow.jpg', 6, 13.5);
        game.load.audio('powerup', 'sound/powerup.mp3');
        game.load.audio('powerdown', 'sound/powerdown.mp3');
        game.load.audio('jump', 'sound/jump.mp3');
    },

    create : function  () {

        this.platforms = [];
        this.location=605;
        this.yellows= [];
        this.lastTime = 0;
        this.distance = 0;
        this.score = 0;
        this.status = 'running';

        this.cursor = game.input.keyboard.addKeys({
            'esc': Phaser.Keyboard.ESC,
            'enter': Phaser.Keyboard.ENTER,
            'up': Phaser.Keyboard.UP,
            'down': Phaser.Keyboard.DOWN,
            'left': Phaser.Keyboard.LEFT,
            'right': Phaser.Keyboard.RIGHT
        });


        //audio
        powerup = game.add.audio('powerup');
        powerdown = game.add.audio('powerdown');
        jump = game.add.audio('jump');


        //background lifebar
        this.background2 = game.add.sprite(0, 0, 'background2');
        this.background = game.add.tileSprite(50, 50, 384, 354, 'background');
        this.life = game.add.sprite(500, 10, 'life');
        for(var i=0;i<12;i++){
            var yellow = game.add.sprite(509+i*8, 33, 'yellow');
            this.yellows.push(yellow);
        }

        
        //UI
        var Button = game.add.button(500 , 330,  "undo", this.stateMenu, this);
        Button.anchor.set(0.5);
        Button = game.add.button(580 , 330, "pause", this.statePause, this);
        Button.frame = 1;
        Button.anchor.set(0.5);

        //sound
        var soundButton = game.add.button(500 , 400,  "soundicons", this.setSound1, this);
        soundButton.anchor.set(0.5);
        soundButton = game.add.button(550 , 400, "soundicons", this.setSound2, this);
        soundButton.frame = 1;
        soundButton.anchor.set(0.5);


        //createWall;
        this.bar1 = game.add.image(26, 50, 'bar1');
        this.leftWall = game.add.sprite(33, 50, 'wall');
        game.physics.arcade.enable(this.leftWall);
        this.leftWall.body.immovable = true;

        this.bar4 = game.add.image(450, 49, 'bar1');
        this.rightWall = game.add.sprite(433, 50, 'wall');
        game.physics.arcade.enable(this.rightWall);
        this.rightWall.body.immovable = true;

        this.bar2 = game.add.image(26, 40, 'bar2');
        this.bar3 = game.add.image(28, 404, 'bar3');
        this.ceiling = game.add.image(50, 50, 'ceiling');


        //createPlayer
        this.player = game.add.sprite(200, 50, 'player');
        this.player.direction = 10;
        game.physics.arcade.enable(this.player);
        this.player.body.gravity.y = 500;
        this.player.animations.add('left', [0, 1, 2, 3], 8);
        this.player.animations.add('right', [9, 10, 11, 12], 8);
        this.player.animations.add('flyleft', [18, 19, 20, 21], 12);
        this.player.animations.add('flyright', [27, 28, 29, 30], 12);
        this.player.animations.add('fly', [36, 37, 38, 39], 12);
        this.player.life = 12;
        this.player.touchOn = undefined;

        //createText
        var style1 = {fill: 'orange', fontSize: '30px'}
        var style2 = {fill: 'red', fontSize: '30px'}
        this.text = game.add.text(180, 5, '', style1);
        this.text2 = game.add.text(140, 200, 'Enter 重新開始', style2);
        this.text2.visible = false;
        this.text3 = game.add.text(180, 200, '遊戲暫停', style2);
        this.text3.visible = false;
    },

    update : function () {

        // bad
        if(this.status == 'gameOver' && this.cursor.enter.isDown) this.restart();
        if(this.status != 'running') return;
        this.physics.arcade.collide(this.player, this.platforms, this.effect);
        this.physics.arcade.collide(this.player, [this.leftWall, this.rightWall]);
        //...
        //this.background.tilePosition.y -= 0.4;
        this.ceilingTouch(this.player);
        this.GameOver();
        this.movePlayer();
        this.movePlatforms();
        this.updateText();
        this.createPlatforms();
    },
    setSound1:function() {
        playSound = true; 
    },
    setSound2:function() {
        playSound = false;
    },
    stateMenu: function() {
        game.state.start('title');
    },
    statePause: function() {
        if(game.paused){
            game.paused=false;
            this.text3.visible = false;
        }
        else{
            game.paused=true;
            this.text3.visible = true;
        }
    },

    
    createPlatforms : function  () {
        var i=this.platforms.length-1;
        var platform = this.platforms[i];
        if(i<0){
            this.lastTime = game.time.now;
            this.createPlatform();
            this.distance += 1;
            if(this.distance%4==0)
            {
                this.score+=1;
            }
        }
        else if(platform.body.position.y < 310) {
            this.lastTime = game.time.now;
            this.createPlatform();
            this.distance += 1;
            if(this.distance%4==0)
            {
                this.score+=1;
            }
        }
    },

    createPlatform : function () {

        var platform;
        var x = Math.random()*(400 - 96 - 40) + 50;
        var y = 380;
        var random = Math.random() * 100;

        if(random < 45) {
            platform = game.add.sprite(x, y, 'normal');
        } 
        else if (random>=45 && random < 60) {
            platform = game.add.sprite(x, y, 'nails');
            game.physics.arcade.enable(platform);
            platform.body.setSize(96, 15, 0, 15);
        } 
        else if (random>=60 && random < 70) {
            platform = game.add.sprite(x, y, 'conveyorLeft');
            platform.animations.add('scroll', [0, 1, 2, 3], 16, true);
            platform.play('scroll');
        } 
        else if (random>=70 && random < 80) {
            platform = game.add.sprite(x, y, 'conveyorRight');
            platform.animations.add('scroll', [0, 1, 2, 3], 16, true);
            platform.play('scroll');
        } 
        else if (random>=80 && random < 90) {
            platform = game.add.sprite(x, y, 'trampoline');
            platform.animations.add('jump', [4, 5, 4, 3, 2, 1, 0, 1, 2, 3], 120);
            platform.frame = 3;
        } 
        else {
            platform = game.add.sprite(x, y, 'fake');
            platform.animations.add('turn', [0, 1, 2, 3, 4, 5, 0], 14);
        }

        game.physics.arcade.enable(platform);
        platform.body.immovable = true;
        this.platforms.push(platform);

        platform.body.checkCollision.down = false;
        platform.body.checkCollision.left = false;
        platform.body.checkCollision.right = false;
    },

    movePlayer : function () {
        if(this.cursor.left.isDown) {
            this.player.body.velocity.x = -250;
        } 
        else if(this.cursor.right.isDown) {
            this.player.body.velocity.x = 250;
        } 
        else {
            this.player.body.velocity.x = 0;
        }

        //Animate
        var x = this.player.body.velocity.x;
        var y = this.player.body.velocity.y;

        if (x < 0 && y > 0) {
            this.player.animations.play('flyleft');
        }
        else if (x > 0 && y > 0) {
            this.player.animations.play('flyright');
        }
        else if (x < 0 && y == 0) {
            this.player.animations.play('left');
        }
        else if (x > 0 && y == 0) {
            this.player.animations.play('right');
        }
        else if (x == 0 && y != 0) {
            this.player.animations.play('fly');
        }
        else if (x == 0 && y == 0) {
            this.player.frame = 8;
        }
    },

    movePlatforms : function () {
        for(var i=0; i<this.platforms.length; i++) {
            var platform = this.platforms[i];
            platform.body.position.y -= 1.2;
            if(platform.body.position.y <= 40) {
                platform.destroy();
                this.platforms.splice(i, 1);
            }
        }
    },

    updateText : function () {
        if(this.score<10){
            this.text.setText('地下000'+ this.score +'階')
        }
        else if(this.score<100){
            this.text.setText('地下00'+ this.score +'階')
        }
        else if(this.score<1000){
            this.text.setText('地下0' + this.score +'階')
        }
        else{
            this.text.setText('地下' + this.score +'階')
        }
    },

    effect : function (player, platform) {
        if(platform.key == 'conveyorRight') {
            player.body.x += 2;
        }
        else if(platform.key == 'conveyorLeft') {
            player.body.x -= 2;
        }
        else if(platform.key == 'trampoline') {
            platform.animations.play('jump');
            player.body.velocity.y = -270;
            if(player.life < 12) {
                player.life += 1;
                var yellow = game.add.sprite(mainState.location, 33, 'yellow');
                mainState.yellows[player.life-1]=yellow;
                mainState.location+=8;
            }
            if(playSound){
                jump.play();
            }
        }
        else if(platform.key == 'nails') {
            if (player.touchOn !== platform) {
                player.touchOn = platform;
                if(playSound){
                    powerdown.play();
                }
                mainState.location-=32; 
                var i;
                for(var i=player.life-1;i>player.life-5;i--)
                {
                    var yellow = mainState.yellows[i];
                    if(i>=0){
                        yellow.destroy();
                    }
                }
                player.life-=4;  
                game.camera.flash(0xff0000, 100);

                //particle
                var x=player.body.position.x,
                    y=player.body.position.y;

                mainState.emitter = game.add.emitter(x, y, 10);
                mainState.emitter.makeParticles('pixel');
                mainState.emitter.setYSpeed(-150, 150);
                mainState.emitter.setXSpeed(-150, 150);
                mainState.emitter.setScale(2, 0, 2, 0, 800);
                mainState.emitter.gravity = 500;  
                mainState.emitter.start(true, 800, null, 10);
                         
            }
        }
        else if(platform.key == 'normal') {
            if (player.touchOn !== platform) {
                if(player.life < 12) {
                    player.life += 1;
                    var yellow = game.add.sprite(mainState.location, 33, 'yellow');
                    mainState.yellows[player.life-1]=yellow;
                    mainState.location+=8;
                    if(playSound){
                        powerup.play();
                    }
                }

                player.touchOn = platform;
            }
        }
        else if(platform.key == 'fake') {
            if(player.touchOn !== platform) {
                platform.animations.play('turn');
                setTimeout(function() {
                    platform.body.checkCollision.up = false;
                }, 100);
                player.touchOn = platform;
            }
        }
    },
    ceilingTouch : function (player) {
        if(player.body.y < 50) {

            //particle
            var x=player.body.position.x,
                y=player.body.position.y;

            this.emitter = game.add.emitter(x, y, 10);
            this.emitter.makeParticles('pixel');
            this.emitter.setYSpeed(-150, 150);
            this.emitter.setXSpeed(-150, 150);
            this.emitter.setScale(2, 0, 2, 0, 800);
            this.emitter.gravity = 500;  
            this.emitter.start(true, 800, null, 10);

            player.body.y+=30;
            if(player.body.velocity.y < 0) {
                player.body.velocity.y = 0;
            }
        
            this.location-=32; 
            var i;
            for(var i=player.life-1;i>player.life-5;i--)
            {
                var yellow = mainState.yellows[i];
                if(i>=0){
                    yellow.destroy();
                }
            }
            player.life-=4; 
            game.camera.flash(0xff0000, 100);
            if(playSound){
                powerdown.play();
            }
        }
    },

    GameOver : function () {
        if(this.player.life <= 0 || this.player.body.y > 400) {
            this.text2.visible = true;
            this.platforms.forEach(function(s) {s.destroy()});
            this.platforms = [];
            this.status = 'gameOver';
            game.state.start('end');
        }
    },
    restart : function () {
        this.text2.visible = false;
        this.distance = 0;
        this.createPlayer();
        this.status = 'running';
    }
}
var game = new Phaser.Game(650, 450, Phaser.AUTO, 'canvas');
game.state.add('title', titleScreen);
game.state.add('score', scoreBoard);
game.state.add('main', mainState);
game.state.add('end', endState);
game.state.start('title');