# Software Studio 2018 Spring Assignment 02 小朋友下樓梯

## 小朋友下樓梯
<img src="example01.png" width="700px" height="500px"></img>

## Goal
1. **Fork the repo ,remove fork relationship and change project visibility to public.**
2. Complete a game "小朋友下樓梯" by Phaser. (JavaScript or TypeScript)
3. Your game should reach the basic requirements.
4. You can download needed materials from some open source webpage to beautify the appearance.
5. Commit to "your" project repository and deploy to Gitlab page.
6. **Report which items you have done and describing other functions or feature in REABME.md.**

## Scoring 
|                                              Item                                              | Score |
|:----------------------------------------------------------------------------------------------:|:-----:|
| A complete game process: start menu => game view => game over => quit or play again            |  20%  |
| Your game should follow the basic rules of  "小朋友下樓梯".                                    |  15%  |
|         All things in your game should have correct physical properties and behaviors.         |  15%  |
| Set up some interesting traps or special mechanisms. .(at least 2 different kinds of platform) |  10%  |
| Add some additional sound effects and UI to enrich your game.                                  |  10%  |
| Store player's name and score in firebase real-time database, and add a leaderboard to your game.        |  10%  |
| Appearance (subjective)                                                                        |  10%  |
| Other creative features in your game (describe on README.md)                                   |  10%  |

## Reminder
* Do not make any change to our root project repository.
* Deploy your web page to Gitlab page, and ensure it works correctly.
    * **Your main page should be named as ```index.html```**
    * **URL should be : https://[studentID].gitlab.io/Assignment_02**
* You should also upload all source code to iLMS.
    * .html or .htm, .css, .js, .ts, etc.
    * source files
* **Deadline: 2018/05/24 23:59 (commit time)**
    * Delay will get 0 point (no reason)
    * Copy will get 0 point
    * "屍體" and 404 is not allowed


## Report 
|                                              Item                                              |  |
|:----------------------------------------------------------------------------------------------:|:-----:|
| A complete game process: start menu => game view => game over => quit or play again            |  Y  |
| Your game should follow the basic rules of  "小朋友下樓梯".                                    |  Y  |
|         All things in your game should have correct physical properties and behaviors.         |  Y  |
| Set up some interesting traps or special mechanisms. .(at least 2 different kinds of platform) |  Y  |
| Add some additional sound effects and UI to enrich your game.                                  |  Y  |
| Store player's name and score in firebase real-time database, and add a leaderboard to your game.        |  Y  |
| Appearance (subjective)                                                                        |    |
| Other creative features in your game (describe on README.md)                                   |  Y  |


**玩法介紹**
* 剛開始玩家先進到選單，會看到兩個選項，一個是start，一個是leaderboard，點選排行榜會看到目前最高的六個名次，右下角有back選項，點擊回到選單，而start則是開始遊戲
* 遊戲右方有四個按鍵，左上方的是放棄遊戲回到選單，右上方則是暫停，而下方的是聲音控制，左邊是開啟音效，右邊是關閉
* 遊戲結束後會出現結算畫面，顯示玩家得分，並可輸入姓名記錄分數，如果分數達到前六名則會出現在排行榜，同時提供兩個選項供玩家選擇，一個是重新遊戲，一個是回到目錄
* 加分項目：遊戲中人物遭到尖刺攻擊會出現紅色粒子特效